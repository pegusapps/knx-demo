.. KNX Documentation Test documentation master file, created by
   sphinx-quickstart on Fri Sep 22 08:31:11 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KNX Documentation Test's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2


Testing
=======

This is a test.

This is some JSON::

 {
   "key":"value"
 }

This is some other text.

.. TIP::
   You can add a tip as well.

.. NOTE:: Notes are fun!
   You can add a note as well.

   This indented line is still part of the note.

   As is this code::
    {
      "note": true
    }

.. IMPORTANT::
   You can add a note as well.


This is a KNX diagram specified in 1 line:

.. knxdiagram:: knx:Building

This is another KNX diagram that is left aligned:

.. knxdiagram::
   :align: left

   knx:LightSwitchBasic

Aligned center:

.. knxdiagram::
   :align: center

   knx:LightSwitchBasic

Aligned right:

.. knxdiagram::
   :align: right

   knx:DPT_Switch_Enum

Test without content (This will print the warning "WARNING: Ignoring "knxdiagram" directive without content" when building the docs):

.. knxdiagram::

Last line of the document.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

