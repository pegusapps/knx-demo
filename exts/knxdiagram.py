# -*- coding: utf-8 -*-
"""
    sphinxcontrib.knxdiagram
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    :copyright: Copyright 2012 by Takeshi KOMIYA
    :license: BSD, see LICENSE for details.
"""

from docutils import nodes
from docutils.parsers.rst import directives

from sphinx.util.compat import Directive
from subprocess import call
from os import path
import shutil
import posixpath
from sphinx.util.osutil import ensuredir
from os import getenv


class knxdiagram(nodes.General, nodes.Element):
    pass

def align_spec(argument):
    # type: (Any) -> bool
    return directives.choice(argument, ('left', 'center', 'right'))

class KnxdiagramDirective(Directive):
    """Directive for evaluating some Python code"""

    has_content = True
    required_arguments = 0
    optional_arguments = 1
    final_argument_whitespace = True
    option_spec = {
        'align': align_spec,
    }

    def run(self):
        node = knxdiagram()
        if 'align' in self.options:
            node['align'] = self.options['align']

        if self.arguments:
            node['query'] = " ".join(self.arguments)
        else:
            node['query'] = '\n'.join(self.content)
            if not node['query'].strip():
                return [self.state_machine.reporter.warning(
                    'Ignoring "knxdiagram" directive without content.',
                    line=self.lineno)]

        return [node]


def visit_knxdiagram_node(self, node):
    relfn = generate_knx_diagram(node, self)

    if 'align' in node:
        self.body.append('<div align="%s" class="align-%s">' %
                         (node['align'], node['align']))

    self.body.append(('<img src="%s"' % relfn) + '/></p>\n' )

    if 'align' in node:
        self.body.append('</div>\n')

def depart_knxdiagram_node(self, node):
    pass

def latex_visit_knxdiagram_node(self, node):
    relfn = generate_knx_diagram(node, self)

    pre = ''
    post = ''
    if 'align' in node:
        if node['align'] == 'left':
            pre = '{'
            post = r'\hspace*{\fill}}'
        elif node['align'] == 'right':
            pre = r'{\hspace*{\fill}'
            post = '}'
        elif node['align'] == 'center':
            pre = r'{\hfill'
            post = r'\hspace*{\fill}}'
    self.body.append('\n%s' % pre)

    self.body.append(r'\includegraphics{%s}' % relfn)

    self.body.append('%s\n' % post)

def generate_knx_diagram(node, self):
    model = node['query']
    print "Creating diagram for model %s" % model
    call(["node", getenv("KNXDIAGRAM_FOLDER", ".") + "/knx-diagram", model, "-s", getenv("WEBSERVER_HOST", "web"), "-p", getenv("WEBSERVER_PORT", "8080")])
    filename = '%s.png' % model
    imgpath = "./images/generated/%s.png" % model
    relfn = posixpath.join(self.builder.imgpath, 'knxdiagram', filename)
    outfn = path.join(self.builder.outdir, self.builder.imagedir, 'knxdiagram', filename)
    ensuredir(path.dirname(outfn))
    shutil.move(imgpath, outfn)
    return relfn


def setup(app):
    app.add_node(knxdiagram,
                 html=(visit_knxdiagram_node, depart_knxdiagram_node),
                 latex=(latex_visit_knxdiagram_node, depart_knxdiagram_node))
    app.add_directive('knxdiagram', KnxdiagramDirective)
